package presenter;

public interface IPresenter {
    void countIt(String first, String second, String lastSing);
    void countSQRT(String str);
    void toLog(String firstPart, String secondPart, String sign, String result);
    String getLog();
    void clearHistory();
    void saveFile();
    String openFile();
}
