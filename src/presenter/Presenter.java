package presenter;

import model.History;
import model.IModel;
import model.Model;
import view.IView;
import view.View;

public class Presenter implements IPresenter{
    private IView view;
    private IModel model;

    public Presenter(IView view){
        this.view = view;
        this.model = new Model();
    }

    @Override
    public void countIt(String first, String second, String lastSing) {
        view.showResult(model.countIt(first,second,lastSing));
    }

    @Override
    public void countSQRT(String str) {
        view.showSQRTResult(model.countSQRT(str));
    }

    @Override
    public void toLog(String firstPart, String secondPart, String sign, String result) {
        History.getHistory().addLine(firstPart,secondPart,sign,result);
    }

    @Override
    public void clearHistory() {
        History.getHistory().clearHistory();
    }

    @Override
    public String getLog() {
        return History.getHistory().getLog();
    }

    @Override
    public void saveFile() {
        History.getHistory().saveFile();
    }

    @Override
    public String openFile() {
        return History.getHistory().openFile();
    }
}
