package model;

public class Engineering {

    public String dwts(String str) {
        Keeper keeper = new Keeper();
        for (String somestr : str.split(" ")) {
            keeper.addEnd(somestr);
        }

        return "";
    }

    private String countIt(String first, String second, String Sing) {
        switch (Sing) {
            case "-":
                return frmt(Double.toString(Double.parseDouble(first) - Double.parseDouble(second)));
            case "+":
                return frmt(Double.toString(Double.parseDouble(first) + Double.parseDouble(second)));
            case "*":
                return frmt(Double.toString(Double.parseDouble(first) * Double.parseDouble(second)));
            case "/":
                return frmt(Double.toString(Double.parseDouble(first) / Double.parseDouble(second)));
            case "√":
                return frmt(String.valueOf(Math.sqrt(Double.parseDouble(first))));
            case "^":
                return frmt(Double.toString(Math.pow(Double.parseDouble(first), Double.parseDouble(second))));
            default:
                return "";
        }
    }

    private String frmt(String str) {
        double d = Double.parseDouble(str);

        if (d == (long) d) {
            return String.format("%d", (long) d);
        } else {
            return String.format("%s", d);
        }
    }

    private String findBkt(String[] strings) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i].contains(")")) {
                for (int j = i; j > 0; j--) {
                    if (strings[j].contains("(")) {
                        Keeper buf = new Keeper();
                        for (int k = j; k < i; k++) {
                            buf.addEnd(strings[k]);
                        }
                    }
                }
            }
        }
        return null;
    }

    private String countable(String[] str){
        String bufBktStr = bufBkt(killTheBkt(str[0])).contains("(") ? bufBkt(killTheBkt(str[0])) : bufBkt(killTheBkt(str[str.length-1]));
        str[0] = str[0].replace(bufBkt(killTheBkt(str[0])), "");
        str[str.length-1] = str[str.length-1].replace(bufBkt(killTheBkt(str[0])), "");
        do {
            for (String s : str) {
                if (s.contains("√") & s.contains("^")){

                }
            }
        }while (isContains(str,"√") || isContains(str,"^"));
        return null;
    }

    private boolean isContains(String[] strings, String sign){
        for (String str: strings) {
            if (str.contains(sign)){
                return true;
            }
        }
        return false;
    }

    private String killTheBkt(String str){
        if (str.contains("(")){
            return str.substring(1);
        } else {
            if (str.contains(")")){
                return str.substring(0, str.length()-1);
            } else {
                return str;
            }
        }
    }

    private String bufBkt(String str){
        if (str.contains("(")){
            return str.substring(str.indexOf("("), str.lastIndexOf("("));
        } else {
            if (str.contains(")")){
                return str.substring(str.indexOf(")"), str.lastIndexOf(")"));
            } else {
                return str;
            }
        }
    }

    private String dwtfs(String str){
        char[] strArray = str.toCharArray();
        String buf = "";
        for (int i = 0; i < strArray.length; i++)  {
            if ((strArray[i] == '√') || (strArray[i] == '^')){
                if (strArray[i] == '√'){
                    int j = i+1;
                    while ((strArray[j] == '√') || (strArray[j] == '^')){
                        buf = buf.concat(String.valueOf(strArray[j]));
                        j++;
                    }
                    String bufStrArray = countIt(buf, null, "√");

                }
            }
        }
        return null;
    }
}