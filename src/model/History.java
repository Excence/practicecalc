package model;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class History {
    private static History history = null;
    private String log = "";
    private String oldLog = "";

    private History(){}

    public static History getHistory(){
        return history = history == null ? new History() : history;
    }

    public void addLine(String firstPart, String secondPart, String sign, String result){
        this.log += firstPart + " " + sign + " " + secondPart+ " = " + result + " \n";
    }

    public String getLog(){
        return this.log;
    }

    public void clearHistory(){
        this.log = "";
    }

    public void saveFile(){
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("C://Users/"+ System.getProperty("user.name") +"/Desktop/"));
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            try {
                Date date = new Date();
                FileWriter fileWriter = new FileWriter(chooser.getSelectedFile()+".txt");
                fileWriter.write(oldLog.equals("") ?
                        new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date) + "\n" + log :
                        oldLog + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date) + "\n" + log );
//                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
//                if (oldLog.equals("")){
//                    bufferedWriter.write(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date));
//                    bufferedWriter.newLine();
//                    writeTo(bufferedWriter, log);
//                } else {
//                    writeTo(bufferedWriter, oldLog);
//                    bufferedWriter.write(new SimpleDateFormat("dd-MM-yyyy HH:mm").format(date));
//                    bufferedWriter.newLine();
//                    writeTo(bufferedWriter, log);
//                }
//                bufferedWriter.flush();
//                bufferedWriter.close();
                fileWriter.flush();
                fileWriter.close();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Проблема с записью в файл");
            }
        }
    }

    public String openFile(){
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
        chooser.setCurrentDirectory(new File("C://Users/"+ System.getProperty("user.name") +"/Desktop/"));
        if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            try {
                FileReader fileReader = new FileReader(chooser.getSelectedFile());
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                while (bufferedReader.ready()){
                    oldLog += bufferedReader.readLine() + "\n";
                }
                bufferedReader.close();
            } catch (Exception ex){
                JOptionPane.showMessageDialog(null, "Файл не читается или не txt");
            }
        }
        return oldLog;
    }

//    private void writeTo(BufferedWriter bufferedWriter, String str) {
//        try {
//            for (String line: str.split("\\n")) {
//                bufferedWriter.write(line);
//                bufferedWriter.newLine();
//            }
//            bufferedWriter.flush();
//        } catch (Exception ex) {
//            JOptionPane.showMessageDialog(null, "Проблема с частичной записью");
//        }
//    }
}
