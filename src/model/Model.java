package model;

public class Model implements IModel {
    @Override
    public String countIt(String first, String second, String lastSing) {
        switch (lastSing){
            case "-" : return frmt(Double.toString(Double.parseDouble(first)-Double.parseDouble(second)));
            case "+" : return frmt(Double.toString(Double.parseDouble(first)+Double.parseDouble(second)));
            case "*" : return frmt(Double.toString(Double.parseDouble(first)*Double.parseDouble(second)));
            case "/" : return frmt(Double.toString(Double.parseDouble(first)/Double.parseDouble(second)));
            default: return  "";
        }
    }

    @Override
    public String countSQRT(String str) {
        return frmt(String.valueOf(Math.sqrt(Double.parseDouble(str))));
    }

    private String frmt(String str){
        double d = Double.parseDouble(str);

        if (d == (long) d){
            return String.format("%d", (long) d);
        } else {
            return String.format("%s", d);
        }
    }
}

