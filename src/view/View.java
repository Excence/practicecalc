package view;

import presenter.IPresenter;
import presenter.Presenter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class View extends JFrame implements IView {

    private  JFrame jFrame;
    private  JPanel jPanel;
    private  String lastSign;
    private  JTextField jTextField;
    private  String firstOperator;
    private  String secondOperator;
    private  JTextField jTextFieldUp;
    private  boolean flag = false;
    private  boolean sqrtFlag = false;
    private  boolean signFlag = true;
    private  IPresenter presenter;

    private void initPresenter(){
        presenter = new Presenter(this);
    }

    private void initView(){
        jFrame = new JFrame("Practice Calc");
        jPanel = new JPanel();
        jPanel.setLayout(null);

        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(315, 435);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);
        jFrame.add(jPanel);

        JMenuBar jMenuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem fileSave = new JMenuItem("Save");
        file.add(fileSave);
        JMenuItem fileOpen = new JMenuItem("Open");
        file.add(fileOpen);
        JMenu history = new JMenu("History");
        JMenuItem openHistory = new JMenuItem("Open");
        history.add(openHistory);
        JMenuItem clearHistory = new JMenuItem("Clear");
        history.add(clearHistory);
        jMenuBar.add(file);
        jMenuBar.add(history);
        jFrame.setJMenuBar(jMenuBar);
        jFrame.revalidate();

        jTextFieldUp = new JTextField();
        jTextFieldUp.setBounds(20, 15, 260, 20);
        jTextFieldUp.setHorizontalAlignment(4);
        jTextFieldUp.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,0),2));
        jTextFieldUp.setEnabled(false);
        jPanel.add(jTextFieldUp);

        jTextField = new JTextField();
        jTextField.setBounds(20, 35, 260, 30);
        jTextField.setHorizontalAlignment(4);
        jTextField.setFont(new Font("" ,Font.BOLD,20));
        jTextField.setBorder(BorderFactory.createLineBorder(new Color(0,0,0,0),2));
        jPanel.add(jTextField);

        JButton jButtonBackspace = new JButton("←");
        jButtonBackspace.setBounds(20, 125, 50, 30);
        jPanel.add(jButtonBackspace);

        JButton jButtonC = new JButton("C");
        jButtonC.setBounds(90,75,50,30);
        jPanel.add(jButtonC);

        JButton jButtonCE = new JButton("CE");
        jButtonCE.setBounds(20,75,50,30);
        jPanel.add(jButtonCE);

        JButton jButton7 = new JButton("7");
        jButton7.setBounds(20, 175, 50, 30);
        jPanel.add(jButton7);

        JButton jButton8 = new JButton("8");
        jButton8.setBounds(90, 175, 50, 30);
        jPanel.add(jButton8);

        JButton jButton9 = new JButton("9");
        jButton9.setBounds(160, 175, 50, 30);
        jPanel.add(jButton9);

        JButton jButton4 = new JButton("4");
        jButton4.setBounds(20, 225, 50, 30);
        jPanel.add(jButton4);

        JButton jButton5 = new JButton("5");
        jButton5.setBounds(90, 225, 50, 30);
        jPanel.add(jButton5);

        JButton jButton6 = new JButton("6");
        jButton6.setBounds(160, 225, 50, 30);
        jPanel.add(jButton6);

        JButton jButton1 = new JButton("1");
        jButton1.setBounds(20, 275, 50, 30);
        jPanel.add(jButton1);

        JButton jButton2 = new JButton("2");
        jButton2.setBounds(90, 275, 50, 30);
        jPanel.add(jButton2);

        JButton jButton3 = new JButton("3");
        jButton3.setBounds(160, 275, 50, 30);
        jPanel.add(jButton3);

        JButton jButton0 = new JButton("0");
        jButton0.setBounds(20, 325, 120, 30);
        jPanel.add(jButton0);

        JButton jButtonDot = new JButton(".");
        jButtonDot.setBounds(160, 325, 50, 30);
        jPanel.add(jButtonDot);

        JButton jButtonPlus = new JButton("+");
        jButtonPlus.setBounds(230, 175, 50, 80);
        jPanel.add(jButtonPlus);

        JButton jButtonMinus = new JButton("-");
        jButtonMinus.setBounds(230, 125, 50, 30);
        jPanel.add(jButtonMinus);

        JButton jButtonMultiply = new JButton("*");
        jButtonMultiply.setBounds(160, 125, 50, 30);
        jPanel.add(jButtonMultiply);

        JButton jButtonDivide = new JButton("/");
        jButtonDivide.setBounds(90, 125, 50, 30);
        jPanel.add(jButtonDivide);

        JButton jButtonPosOrNeg = new JButton("±");
        jButtonPosOrNeg.setBounds(230, 75, 50, 30);
        jPanel.add(jButtonPosOrNeg);

        JButton jButtonEqual = new JButton("=");
        jButtonEqual.setBounds(230, 275, 50, 80);
        jPanel.add(jButtonEqual);

        JButton jButtonSqrt = new JButton("√");
        jButtonSqrt.setBounds(160, 75, 50, 30);
        jPanel.add(jButtonSqrt);

        jFrame.revalidate();
        jFrame.setVisible(true);

        jButton0.addActionListener(e -> {
            String buf = jTextField.getText();
            if (!jTextField.getText().equals("0")) {
                buf += "0";
                jTextField.setText(buf);
            }
        });

        jButton1.addActionListener(this::addDigit);

        jButton2.addActionListener(this::addDigit);

        jButton3.addActionListener(this::addDigit);

        jButton4.addActionListener(this::addDigit);

        jButton5.addActionListener(this::addDigit);

        jButton6.addActionListener(this::addDigit);

        jButton7.addActionListener(this::addDigit);

        jButton8.addActionListener(this::addDigit);

        jButton9.addActionListener(this::addDigit);

        jButtonBackspace.addActionListener(e -> {
            if (jTextField.getText().equals("") || jTextField.getText().equals("0")){
                jTextField.setText("0");
            } else {
                String buf = jTextField.getText();
                buf = buf.substring(0, buf.length() - 1);
                jTextField.setText(buf);
            }
        });

        jButtonDot.addActionListener(e -> {
            String buf = jTextField.getText();
            if (buf.length() == 0){
                buf = "0.";
                jTextField.setText(buf);
            } else {
                if (buf.contains(".")){
                    jTextField.setText(buf);
                } else {
                    buf += ".";
                    jTextField.setText(buf);
                }
            }
        });

        jButtonPosOrNeg.addActionListener(e -> {
            String buf = jTextField.getText();
            if(buf.contains("-")){
                buf = buf.replace("-", "");
                jTextField.setText(buf);
            } else {
                buf = "-" + buf;
                jTextField.setText(buf);
            }

        });



        jButtonPlus.addActionListener(e -> addSign(e, presenter));

        jButtonMultiply.addActionListener(e -> addSign(e, presenter));

        jButtonDivide.addActionListener(e -> addSign(e, presenter));

        jButtonMinus.addActionListener(e -> addSign(e, presenter));

        jButtonC.addActionListener(e -> {
            jTextFieldUp.setText("");
            jTextField.setText("");
            firstOperator = "0";
            secondOperator = "0";
            lastSign = "+";
        });

        jButtonCE.addActionListener(e -> {
            jTextField.setText("0");
            firstOperator = "0";
        });

        jButtonEqual.addActionListener(e -> {
            if (!jTextFieldUp.getText().equals("")){
                jTextFieldUp.setText("");
                secondOperator = jTextField.getText();
                presenter.countIt(firstOperator, secondOperator, lastSign);
                presenter.toLog(firstOperator, secondOperator, lastSign, jTextField.getText());
                firstOperator = jTextField.getText();
            } else {
                presenter.countIt(firstOperator, secondOperator, lastSign);
                presenter.toLog(firstOperator, secondOperator, lastSign, jTextField.getText());
                firstOperator = jTextField.getText();
            }
            flag = true;
            sqrtFlag = false;
        });

        jButtonSqrt.addActionListener(e -> {
            if (!jTextField.getText().startsWith("-")){
                if (!jTextField.getText().equals("")){
                    presenter.countSQRT(jTextField.getText());
                    presenter.toLog("sqrt(" + jTextFieldUp.getText().substring(jTextFieldUp.getText().lastIndexOf("(") + 1,
                            jTextFieldUp.getText().lastIndexOf(")")) +
                            ")", "", "", jTextField.getText());
                    sqrtFlag = true;
                    flag = true;
                }
            } else {
                JOptionPane.showMessageDialog(null, "Я еще не запилил мнимую единицу \n" +
                        "Так что пока без отрицательных корней");
            }
        });

        openHistory.addActionListener(e -> new MyDialog("History", presenter.getLog()));

        clearHistory.addActionListener(e -> presenter.clearHistory());

        fileSave.addActionListener(e -> {
            Thread thread = new Thread(presenter::saveFile);
            thread.start();
        });

        fileOpen.addActionListener(e -> new MyDialog("Previous history", presenter.openFile()));
    }

    public static void main(String[] args) {
        View view = new View();
        view.initPresenter();
        view.initView();

    }

    @Override
    public void showResult(String result) {
        jTextField.setText(result);
    }

    @Override
    public void showSQRTResult(String result) {
        jTextFieldUp.setText(jTextFieldUp.getText()+ "sqrt(" + jTextField.getText() + ")");
        jTextField.setText(result);
    }

    private  void addDigit(ActionEvent e) {
        if (flag) {
            jTextField.setText(e.getActionCommand());
            flag = false;
        } else {
            String buf = jTextField.getText();
            if (buf.equals("0")) {
                jTextField.setText(e.getActionCommand());
            } else {
                buf += e.getActionCommand();
                jTextField.setText(buf);
            }
        }
        signFlag = true;
    }

    private  void addSign(ActionEvent e, IPresenter presenter) {
        if (signFlag) {
            if (!jTextField.getText().equals("")) {
                if (sqrtFlag) {
                    if (jTextFieldUp.getText().startsWith("s")) {
                        firstOperator = jTextField.getText();
                        jTextFieldUp.setText(jTextFieldUp.getText() + " " + e.getActionCommand());
                        lastSign = e.getActionCommand();
                        jTextField.setText("");
                        sqrtFlag = false;
                        signFlag = false;
                    } else {
                        secondOperator = jTextField.getText();
                        jTextFieldUp.setText(jTextFieldUp.getText() + " " + e.getActionCommand());
                        presenter.countIt(firstOperator, secondOperator, lastSign);
                        presenter.toLog(firstOperator, secondOperator, lastSign, jTextField.getText());
                        lastSign = e.getActionCommand();
                        firstOperator = jTextField.getText();
                        sqrtFlag = false;
                        signFlag = false;
                    }
                } else {
                    String buf = jTextFieldUp.getText();
                    if ((!jTextFieldUp.getText().equals("")) && (!jTextField.getText().equals(""))) {
                        secondOperator = jTextField.getText();
                        jTextFieldUp.setText(jTextFieldUp.getText() + " " + jTextField.getText() + " " + e.getActionCommand());
                        presenter.countIt(firstOperator, secondOperator, lastSign);
                        presenter.toLog(firstOperator, secondOperator, lastSign, jTextField.getText());
                        lastSign = e.getActionCommand();
                        firstOperator = jTextField.getText();
                        flag = true;
                        signFlag = false;
                    } else {
                        if (buf.endsWith("/") || buf.endsWith("*") || buf.endsWith("+") || buf.endsWith("-")) {
                            jTextFieldUp.setText(jTextFieldUp.getText().substring(0, buf.length() - 1) + e.getActionCommand());
                            lastSign = e.getActionCommand();
                            signFlag = false;
                        } else {
                            firstOperator = jTextField.getText();
                            jTextFieldUp.setText(jTextField.getText() + " " + e.getActionCommand());
                            lastSign = e.getActionCommand();
                            jTextField.setText("");
                            signFlag = false;
                        }
                    }
                }
            }
        } else {
            String buf = jTextFieldUp.getText();
            if (buf.endsWith("/") || buf.endsWith("*") || buf.endsWith("+") || buf.endsWith("-")) {
                jTextFieldUp.setText(jTextFieldUp.getText().substring(0, buf.length() - 1) + e.getActionCommand());
                lastSign = e.getActionCommand();
                signFlag = false;
            }
        }
    }
}