package view;

public interface IView {
    void showResult(String result);
    void showSQRTResult(String result);
}
