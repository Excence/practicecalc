package view;

import javax.swing.*;

class MyDialog extends JDialog {

     MyDialog(String frameName, String str){
        super(new JFrame(), frameName, true);
        if (!str.equals("")){
           JTextArea jTextArea = new JTextArea();
           jTextArea.setText(str);
           jTextArea.setEditable(false);
           add(new JScrollPane(jTextArea));
           setSize(300, 400);
           setLocationRelativeTo(null);
           setResizable(false);
           setVisible(true);
        } else {
           JOptionPane.showMessageDialog(null, "Ow, such empty");
        }
    }
}
